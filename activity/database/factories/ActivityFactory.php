<?php

namespace Database\Factories;

use App\Models\Activity;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * Class ActivityFactory
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class ActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Activity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'url' => $this->faker->url,
            'count_visit' => $this->faker->numberBetween(1, 999),
            'last_visit' => $this->faker->date
        ];
    }
}
