<?php

namespace Tests\Feature;

use Tests\TestCase;

/**
 * Class ActivityTest
 * @package Tests\Feature
 */
class ActivityTest extends TestCase
{
    /**
     * Get activities.
     *
     * @return void
     */
    public function test_get_activities()
    {
        $response = $this->postJson('/api/activity');

        $response->assertStatus(200);
    }
}
