<?php

use App\Http\Controllers\Api\ActivityController;
use App\Services\Server\JsonRpcServer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
 * Activity routes
 */
Route::post('/activity', function (Request $request, JsonRpcServer $server, ActivityController $controller) {
    return $server->handle($request, $controller);
});
