<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Activity model
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class Activity extends Model
{
    use HasFactory;

    protected $fillable = ['url', 'count_visit', 'last_visit'];

    /**
     * {@inheritDoc}
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($activity) {
            $activity->last_visit = new Carbon(time());
            $activity->count_visit = 1;
        });
    }
}
