<?php

namespace App\Repositories;

use App\Models\Activity;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;

/**
 * Activity repository
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class ActivityRepository
{
    /**
     * Get all activities rows
     *
     * @param int $page page
     * @param int $perPage page per page
     *
     * @return LengthAwarePaginator
     */
    public function all($page = 1, int $perPage = 10):? LengthAwarePaginator
    {
        return Activity::orderBy('count_visit', 'DESC')
            ->paginate($perPage, ['*'], 'page', $page)
            ->withPath(env('FRONTEND_URL'));
    }

    /**
     * Update activity
     *
     * @param array $params params json rpc
     *
     * @return mixed
     */
    public function update(array $params)
    {
        if (!$this->validate($params)) {
            throw new \InvalidArgumentException(trans('validation.custom.invalid-arguments'));
        }

        return Activity::updateOrCreate($params, [
            'last_visit' => new Carbon(time()),
        ])->increment('count_visit');
    }

    /**
     * Check validate
     *
     * @param array $params rpc request params
     *
     * @return bool
     */
    private function validate(array $params): bool
    {
        $validator = Validator::make($params, [
            'url' => 'required|string'
        ]);

        return !$validator->fails();
    }
}
