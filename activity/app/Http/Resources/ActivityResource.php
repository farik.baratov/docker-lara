<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

/**
 * Class ActivityResource
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class ActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'url' => $this->url,
            'count_visit' => $this->count_visit,
            'last_visit' => $this->last_visit
        ];
    }
}
