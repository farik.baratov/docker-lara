<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ActivityResource;
use App\Repositories\ActivityRepository;
use Illuminate\Support\Facades\Log;

/**
 * Activity controller
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class ActivityController extends Controller
{
    /**
     * @var ActivityRepository
     */
    protected $repository;

    /**
     * ActivityController constructor.
     *
     * @param ActivityRepository $repository activity repository
     */
    public function __construct(ActivityRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get activities list
     *
     * @return mixed
     */
    public function activities(array $params = [])
    {
        $data = $this->repository->all($params['page'] ?? 1);

        return $data->setCollection($data->getCollection()->transform(function($product) {
            return new ActivityResource($product);
        }));
    }

    /**
     * Update activity
     *
     * @param array $params json rpc params
     *
     * @return mixed
     */
    public function track(array $params)
    {
        try {
            return $this->repository->update($params);
        } catch (\InvalidArgumentException $e) {
            /*
             * Можно создать абстрактный класс по типу 'KernelException' наследованный от \Exception
             * После чего все созданные exceptions наследовать от KernelException который логирует информацию
             * Что-бы не плодить код т.к. как снизу
             */
            Log::error(sprintf(
                '[Activity Controller TRACK] %s %s',
                $e->getMessage(),
                $e->getTraceAsString()
            ));

            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }
}
