<?php

namespace App\Services\Server\Contracts;

/**
 * Response methods
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
interface ResponseInterface
{
    /**
     * Success response
     *
     * @param mixed $result Result
     * @param string|null $id Identification
     *
     * @return mixed
     */
    public static function success($result, string $id = null);

    /**
     * Error response
     *
     * @param mixed $error Error
     *
     * @return mixed
     */
    public static function error($error);
}
