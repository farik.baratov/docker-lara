<?php

namespace App\Services\Server\Contracts;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

/**
 * Server interface
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
interface ServerInterface
{
    /**
     * Bind controller method to passed ones params and returns response with generated
     * JsonRpcResponse class in format of JSON-RPC 2.0 specification
     *
     * @param Request $request Request
     * @param Controller $controller Controller
     *
     * @return mixed
     */
    public function handle(Request $request, Controller $controller);
}
