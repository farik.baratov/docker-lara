<?php

namespace App\Services\Server\Exceptions;

/**
 * Json Rpc Exception
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class JsonRpcException extends \Exception
{
    /**
     * Code error
     */
    const PARSE_ERROR = 500;
}
