<?php

namespace App\Services\Server;

use App\Services\Server\Contracts\ServerInterface;
use App\Services\Server\Http\Responses\JsonRpcResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Services\Server\Exceptions\JsonRpcException;

/**
 * This json rpc server
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
final class JsonRpcServer implements ServerInterface
{
    /**
     * {@inheritdoc}
     */
    public function handle(Request $request, Controller $controller)
    {
        try {
            $content = json_decode($request->getContent(), true);

            if (empty($content)) {
                throw new JsonRpcException('Parse error', JsonRpcException::PARSE_ERROR);
            }

            $result = $controller->{$content['method']}(...[$content['params']]);

            return JsonRpcResponse::success($result, $content['id']);
        } catch (\Exception $e) {
            return JsonRpcResponse::error($e->getMessage());
        }
    }
}
