<?php

namespace App\Services\Server\Http\Responses;

use App\Services\Server\Contracts\ResponseInterface;

/**
 * Class JsonRpcResponse
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class JsonRpcResponse implements ResponseInterface
{
    /**
     * json-rpc version
     */
    const JSON_RPC_VERSION = '2.0';

    /**
     * {@inheritdoc}
     */
    public static function success($result, string $id = null)
    {
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'result' => $result,
            'id' => $id,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function error($error)
    {
        return [
            'jsonrpc' => self::JSON_RPC_VERSION,
            'error' => $error,
            'id' => null,
        ];
    }
}
