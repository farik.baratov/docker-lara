<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Home</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css">
</head>

<body>
<div class="container">
    <header class="blog-header py-3">
        <div class="nav-scroller py-1 mb-2">
            <nav class="nav d-flex justify-content-between">
                <a class="p-2 text-muted" href="/">Главная</a>
                <a class="p-2 text-muted" href="/about">О нас</a>
                <a class="p-2 text-muted" href="/blog">Блог</a>
                <a class="p-2 text-muted" href="/help">Помощь</a>
                <a class="p-2 text-muted" href="/services">Услуги</a>
                <a class="p-2 text-muted" href="/contacts">Контакты</a>
            </nav>
        </div>
    </header>

    @yield('content')
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
