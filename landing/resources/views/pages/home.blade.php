@extends('layouts/layout')

@section('content')
    <div class="jumbotron p-3 p-md-5 text-white rounded bg-dark">
        <div class="col-md-12 px-0">
            <h1 class="display-4 font-italic">Вы на главной странице!</h1>
            <p class="lead my-3">Поздравляем <3</p>
            <p class="lead mb-0"><a href="/about" class="text-white font-weight-bold">Читайте о нас</a></p>
        </div>
    </div>
    <div class="col-md-12 mt-5">
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
    </div>
@endsection
