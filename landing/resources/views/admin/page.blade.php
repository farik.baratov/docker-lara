<?php
//    dd($activities);
//    ?>
@extends('layouts/layout')

@section('content')
    <div class="container mt-5">
        <table class="table table-bordered mb-5">
            <thead>
            <tr class="table-success">
                <th scope="col">Url</th>
                <th scope="col">Count visit</th>
                <th scope="col">Last visit</th>
            </tr>
            </thead>
            <tbody>
            @foreach($activities as $data)
                <tr>
                    <th scope="row">{{ $data['url'] }}</th>
                    <td>{{ $data['count_visit'] }}</td>
                    <td>{{ $data['last_visit'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{-- Pagination --}}
        <div class="d-flex justify-content-center">
            {{ $activities->links('pagination::bootstrap-4') }}
        </div>
    </div>
@endsection

