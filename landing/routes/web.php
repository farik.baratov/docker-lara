<?php

use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'track',
], function () {
    Route::get('/', [SiteController::class, 'home']);
    Route::get('/about', [SiteController::class, 'about']);
    Route::get('/blog', [SiteController::class, 'blog']);
    Route::get('/help', [SiteController::class, 'help']);
    Route::get('/services', [SiteController::class, 'services']);
    Route::get('/contacts', [SiteController::class, 'contacts']);
});

Route::get('/admin/activity', [SiteController::class, 'activities']);
