<?php

namespace App\Concerns;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

/**
 * Trait Paginatable
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
trait Paginatable
{
    /**
     * Transform to paginate
     *
     * @param array $paginate array pagination
     * @param array $options options
     *
     * @return LengthAwarePaginator
     */
    public function paginate(array $paginate, array $options = []):? LengthAwarePaginator
    {
        $page = $paginate['current_page'] ?? (Paginator::resolveCurrentPage() ?: 1);
        $items = $paginate['data'] ?? Collection::make($paginate['data']);
        $total = $paginate['total'] ?? $items->count();
        $perPage = $paginate['per_page'] ?? 10;

        return new LengthAwarePaginator(
            $items ?? $items->forPage($page, $perPage),
            $total,
            $perPage,
            $page,
            $options
        );
    }
}
