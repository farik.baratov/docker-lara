<?php

namespace App\Services\Client\Contracts;

/**
 * Client interface
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
interface ClientInterface
{
    /**
     * Send to api (json rpc)
     *
     * @param string $method controller method
     * @param array $params req params
     *
     * @return array
     */
    public function send(string $method, array $params): array;
}
