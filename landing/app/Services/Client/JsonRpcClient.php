<?php

namespace App\Services\Client;

use App\Services\Client\Contracts\ClientInterface;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

/**
 * Rpc client
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class JsonRpcClient implements ClientInterface
{
    const JSON_RPC_VERSION = '2.0';

    /**
     * @var Client
     */
    protected $client;

    /**
     * JsonRpcClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'headers' => ['Content-Type' => 'application/json'],
            'base_uri' => config('services.data.base_uri'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function send(string $method, array $params): array
    {
        $response = $this->client
            ->post('activity', [
                RequestOptions::JSON => [
                    'jsonrpc' => self::JSON_RPC_VERSION,
                    'id' => time(),
                    'method' => $method,
                    'params' => $params
                ]
            ])->getBody()->getContents();

        return json_decode($response, true);
    }
}
