<?php

namespace App\Http\Controllers;

use App\Concerns\Paginatable;
use App\Services\Client\JsonRpcClient;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

/**
 * Site controller
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class SiteController extends Controller
{
    use Paginatable;

    /**
     * @var JsonRpcClient
     */
    protected $client;

    /**
     * SiteController constructor.
     *
     * @param JsonRpcClient $client rpc client
     */
    public function __construct(JsonRpcClient $client)
    {
        $this->client = $client;
    }

    /*
     * Для быстрого примера, был выбран вариант простые blade страницы,
     * можно было-бы создать таблицу `pages` на бэке и так-же отдавать информацию о нем через RPC.
     */

    /**
     * Page home
     *
     * @return Application|Factory|View
     */
    public function home()
    {
        return view('pages.home');
    }

    /**
     * Page about
     *
     * @return Application|Factory|View
     */
    public function about()
    {
        return view('pages.about');
    }

    /**
     * Page blog
     *
     * @return Application|Factory|View
     */
    public function blog()
    {
        return view('pages.blog');
    }

    /**
     * Page help
     *
     * @return Application|Factory|View
     */
    public function help()
    {
        return view('pages.help');
    }

    /**
     * Page services
     *
     * @return Application|Factory|View
     */
    public function services()
    {
        return view('pages.services');
    }

    /**
     * Page contacts
     *
     * @return Application|Factory|View
     */
    public function contacts()
    {
        return view('pages.contacts');
    }

    /**
     * Get activities (json-rpc)
     *
     * @param Request $request req
     *
     * @return Application|Factory|View
     */
    public function activities(Request $request)
    {
        $data = $this->client->send('activities', [
            'page' => ($request->page ?? 1)
        ]);

        if (empty($result = $data['result'])) {
            abort(404);
        }

        return view('admin.page', [
            'activities' => $this->paginate($result)
        ]);
    }
}
