<?php

namespace App\Http\Middleware;

use App\Services\Client\JsonRpcClient;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class TrackActivity
 *
 * @author Farukh Baratov <seniorsngstaff@mail.ru>
 * @version 1.0
 */
class TrackActivity
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $client = new JsonRpcClient();

        $data = $client->send('track', [
            'url' => $request->fullUrl(),
        ]);

        # Logging
        if (!empty($result = $data['result']) && isset($result['success']) && !$result['success']) {
            Log::error($result['message']);
        }

        return $next($request);
    }
}
