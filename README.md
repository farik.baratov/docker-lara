# Laravel Docker (test app)

## Требования

Желательные требования к ПО для работы с проектом:

* IDE PHPSTORM
* Docker Windows | Toolbox

Для лучшей работы измените `hosts` файл и впишите следующее:

`127.0.0.1 backend.local`

## Подсказки

Url адреса:

* 127.0.0.1 (backend.local) - бэкэнд (activity)
* 127.0.0.1:8100 - фронтенд (landing)

## Начало работы

После клонирования проекта из репозитория необходимо провести настройку окружения:

1 - Запустить контейнеры:

`docker-compose up -d`

2 - Выполнить установку пакетов в 2х приложениях:

`docker exec activity composer update`

`docker exec landing composer update`

3 - В обоих приложениях необходимо изменить файл:

`.env.example => .env`

4 - Сгенерировать ключи:

`docker exec activity php artisan key:generate`

`docker exec landing php artisan key:generate`

5 - Запустить скрипт засева данных (если необходимо):

`docker exec activity php artisan db:seed`

### По желанию запуск теста

`docker exec activity php artisan test`